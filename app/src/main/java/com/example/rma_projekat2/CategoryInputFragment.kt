package com.example.rma_projekat2

import android.opengl.Visibility
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.database.entites.Category
import com.example.rma_projekat2.database.entites.CategoryType
import com.example.rma_projekat2.database.entites.Trait
import com.example.rma_projekat2.databinding.FragmentCategoryBinding
import com.example.rma_projekat2.databinding.FragmentCategoryInputBinding
import kotlinx.android.synthetic.main.fragment_category_input.*

/**
 * A simple [Fragment] subclass.
 */
class CategoryInputFragment : Fragment() {

    private var traitAdded = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentCategoryInputBinding>(
            inflater, R.layout.fragment_category_input, container,false
        )
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.category_title)


        binding.addTraitButton.setOnClickListener { v: View ->
            if(traitAdded) {
                binding.traitName.visibility = View.GONE
                binding.addTraitButton.text = "Add trait"
                traitAdded = false
            } else {
                binding.traitName.visibility = View.VISIBLE
                binding.addTraitButton.text = "Remove trait"
                traitAdded = true
            }
        }

        binding.doneButton.setOnClickListener { v: View ->
            val db: DB = DB.getInstance(requireContext())
            val categoryName: String = binding.categoryName.text.toString()
            var categoryType = ""
            var categoryId = 0
            when(binding.categoryRadioGroup.checkedRadioButtonId) {
                R.id.incremental_radio -> {
                    categoryType = "Inkrementalna"
                    categoryId = 1
                }
                R.id.timed_radio -> {
                    categoryType = "Vremenska"
                    categoryId = 2
                }
                R.id.quantity_radio -> {
                    categoryType = "Količinska"
                    categoryId = 3
                }
            }
            lateinit var category: Category
            category = if(traitAdded) {
                val traitName = binding.traitName.text.toString()
                Category(0, categoryName,CategoryType(categoryId, categoryType), Trait(0, traitName) )
            } else {
                Category(0, categoryName,CategoryType(categoryId, categoryType), null )
            }
            db.mainDAO().insertCategory(category)

            v.findNavController().navigate(
                R.id.action_categoryInputFragment_to_categoryFragment
            )
        }
        return binding.root;
    }

}
