package com.example.rma_projekat2.insertActivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.rma_projekat2.LocalViewModel
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.databinding.FragmentInsertIncrementalBinding


class InsertIncrementalFragment : Fragment() {
    private val model: LocalViewModel by activityViewModels()
    private lateinit var db: DB
    private var step: Int = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentInsertIncrementalBinding>(
            inflater,
            R.layout.fragment_insert_incremental,
            container,
            false
        )
        db = DB.getInstance(requireContext())
        val activity = model.getInsertedActivity()
        if (activity.category.trait != null) {
            binding.trait.visibility = View.VISIBLE
            binding.trait.text = activity.category.trait.name
        }

        binding.insertButton.setOnClickListener { v ->
            step = binding.stepInput.text.toString().toInt()
            if (binding.trait.isChecked) {
                activity.hasTrait = true
            }
            activity.step = step
            activity.value = 0.0
            db.mainDAO().insertActivity(activity)
            v.findNavController()
                .navigate(R.id.action_insertIncrementalFragment_to_categoryFragment)
        }
        return binding.root
    }
}