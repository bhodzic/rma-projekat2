package com.example.rma_projekat2.insertActivity

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.rma_projekat2.LocalViewModel
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.databinding.FragmentInsertTimeBinding

class InsertTimeFragment : Fragment() {
    private val model: LocalViewModel by activityViewModels()
    private lateinit var db: DB

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentInsertTimeBinding>(
            inflater,
            R.layout.fragment_insert_time,
            container,
            false
        )
        binding.trait.visibility = View.GONE
        db = DB.getInstance(requireContext())
        val activity = model.getInsertedActivity()
        if (activity.category.trait != null) {
            Toast.makeText(requireContext(), "ima osobinu", Toast.LENGTH_LONG).show()
            binding.trait.visibility = View.VISIBLE
            binding.trait.text = activity.category.trait.name
        }

        binding.insertButton.setOnClickListener { v ->
            if (binding.trait.isChecked) {
                activity.hasTrait = true
            }
            activity.start = System.currentTimeMillis()
            db.mainDAO().insertActivity(activity)
            v.findNavController().navigate(R.id.action_insertTimeFragment_to_categoryFragment)
        }
        return binding.root
    }
}