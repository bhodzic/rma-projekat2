package com.example.rma_projekat2.insertActivity

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.rma_projekat2.LocalViewModel
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.database.entites.Activity
import com.example.rma_projekat2.database.entites.Category
import com.example.rma_projekat2.databinding.FragmentInsertActivityBinding

class InsertActivityFragment : Fragment() {

    private lateinit var db: DB
    private lateinit var nextButton: Button
    private lateinit var selected: Category
    private lateinit var categories: ArrayList<Category>
    private lateinit var spinner: Spinner
    private lateinit var activityName: String
    private val model: LocalViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentInsertActivityBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_insert_activity, container, false)

        db = DB.getInstance(requireContext())
        nextButton = binding.nextButton
        spinner = binding.spinner
        populateSpinner()
        nextButton.setOnClickListener { v ->
            activityName = binding.activityNameInput.text.toString()
            model.setInsertedActivity((Activity(0, activityName, null, null, null, null, null, false,  selected)))
            Log.i("bezze", model.getInsertedActivity().toString())
            when (selected.category_type.id) {
                1 -> {
                    v.findNavController().navigate(R.id.action_insertActivityFragment_to_insertIncrementalFragment)
                }
                2 -> {
                    v.findNavController().navigate(R.id.action_insertActivityFragment_to_insertTimeFragment)
                }
                3 -> {
                    v.findNavController().navigate(R.id.action_insertActivityFragment_to_insertValueFragment)
                }
            }
        }

        return binding.root
    }

    private inner class Listener : AdapterView.OnItemClickListener,
        AdapterView.OnItemSelectedListener {
        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {}

        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            selected = categories[position]
        }
    }

    private fun populateSpinner() {
        categories = db.mainDAO().getCategories() as ArrayList<Category>
        val adapter =
            ArrayAdapter(requireContext(), R.layout.spinner_item, categories.map { it.name })
        spinner.adapter = adapter
        spinner.onItemSelectedListener = Listener()
    }

}