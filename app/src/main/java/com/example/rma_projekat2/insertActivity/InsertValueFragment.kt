package com.example.rma_projekat2.insertActivity

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.rma_projekat2.LocalViewModel
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.databinding.FragmentInsertValueBinding

class InsertValueFragment : Fragment() {
    private lateinit var db: DB
    private var value: Double = 0.0
    private var unit: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentInsertValueBinding>(
            inflater,
            R.layout.fragment_insert_value,
            container,
            false
        )
        val model: LocalViewModel by activityViewModels()
        db = DB.getInstance(requireContext())
        val activity = model.getInsertedActivity()
        if (activity.category.trait != null) {
            binding.trait.visibility = View.VISIBLE
            binding.trait.text = activity.category.trait.name
        }

        binding.insertButton.setOnClickListener { v ->
            if (binding.trait.isChecked) {
                activity.hasTrait = true
            }
            value = binding.valueInput.text.toString().toDouble()
            unit = binding.unitInput.text.toString()
            activity.value = value
            activity.measurementUnit = unit
            db.mainDAO().insertActivity(activity)
            v.findNavController().navigate(R.id.action_insertValueFragment_to_categoryFragment)
        }

        return binding.root
    }
}