package com.example.rma_projekat2.activityList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.entites.Activity
import kotlinx.android.synthetic.main.incremental_activity_item.view.*

class IncrementalActivityAdapter(
    private val activities: List<Activity>,
    private val listener: (Activity, Boolean) -> Unit
) : RecyclerView.Adapter<IncrementalActivityAdapter.Holder>() {
    private lateinit var currentActivity: Activity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.incremental_activity_item,
            parent, false
        )

        return Holder(itemView)
    }

    override fun getItemCount(): Int {
        return activities.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        currentActivity = activities[position]

        holder.bind(currentActivity)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.incremental_activity_name
        val value: TextView = itemView.incremental_activity_value
        private val plusButton: Button = itemView.plus_button
        private val minusButton: Button = itemView.minus_button

        fun bind(activity: Activity) {
            name.text = activity.name
            value.text = activity.value.toString()
            plusButton.setOnClickListener {
                listener(currentActivity, true)
            }
            minusButton.setOnClickListener {
                listener(currentActivity, false)
            }
        }

    }
}