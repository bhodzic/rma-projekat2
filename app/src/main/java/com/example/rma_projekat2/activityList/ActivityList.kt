package com.example.rma_projekat2.activityList

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rma_projekat2.LocalViewModel
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.database.entites.Category
import com.example.rma_projekat2.databinding.FragmentActivityListBinding
import kotlinx.android.synthetic.main.fragment_activity_list.*
import kotlinx.android.synthetic.main.value_activity_item.view.*

class ActivityList : Fragment() {
    private val model: LocalViewModel by activityViewModels()
    private lateinit var category: Category
    private lateinit var db: DB

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentActivityListBinding>(inflater,
            R.layout.fragment_activity_list, container, false)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.activities)

        db = DB.getInstance(requireContext())
        category = model.getCurrentCategory()
        return binding.root 
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activities = db.mainDAO().getActivitiesByCategory(category.id)

        when (category.category_type.id) {
            1 -> {
                activity_recycler.adapter = IncrementalActivityAdapter(activities) { activity, inc ->
                    if (inc) {
                        activity.value = activity.value?.plus(activity.step!!)
                    } else {
                        activity.value = activity.value?.minus(activity.step!!)
                    }
                    db.mainDAO().updateActivity(activity)
                    activity_recycler.adapter?.notifyDataSetChanged()
                }
            }
            2 -> {
                activity_recycler.adapter = TimeActivityAdapter(activities) { activity ->
                    activity.stop = System.currentTimeMillis()
                    db.mainDAO().updateActivity(activity)
                    activity_recycler.adapter?.notifyDataSetChanged()
                }
            }
            3 -> {
                activity_recycler.adapter = ValueActivityAdapter(activities)
            }
        }
        activity_recycler.layoutManager = LinearLayoutManager(requireContext())
        activity_recycler.setHasFixedSize(true)
    }
}