package com.example.rma_projekat2.activityList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.database.entites.Activity
import com.example.rma_projekat2.database.entites.Category
import kotlinx.android.synthetic.main.time_activity_item.view.*
import java.sql.Timestamp

class TimeActivityAdapter(
    private val activities: List<Activity>,
    private val listener: (Activity) -> Unit
) : RecyclerView.Adapter<TimeActivityAdapter.Holder>() {
    private lateinit var currentActivity: Activity
    private lateinit var db: DB

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.time_activity_item,
            parent, false
        )

        return Holder(itemView)
    }

    override fun getItemCount(): Int {
        return activities.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        currentActivity = activities[position]

        holder.bind(currentActivity)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.time_activity_name
        val start: TextView = itemView.time_activity_start
        val end: TextView = itemView.time_activity_end
        val stop: Button = itemView.stop_button

        fun bind(activity: Activity) {
            name.text = activity.name
            start.text = Timestamp(activity.start!!).toString()
            if (activity.stop == null) {
                end.text = "Still going..."
            } else {
                end.text = Timestamp(activity.stop!!).toString()
                stop.visibility = View.GONE
            }
            stop.setOnClickListener {
                listener(currentActivity)
                stop.visibility = View.GONE
            }
        }

    }
}