package com.example.rma_projekat2.activityList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_projekat2.R
import com.example.rma_projekat2.database.entites.Activity
import kotlinx.android.synthetic.main.value_activity_item.view.*

class ValueActivityAdapter(private val activities: List<Activity>): RecyclerView.Adapter<ValueActivityAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.value_activity_item,
            parent, false
        )

        return Holder(itemView)
    }

    override fun getItemCount(): Int {
        return activities.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val currentActivity = activities[position]

        holder.bind(currentActivity)
    }

    class Holder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.value_activity_name
        val value: TextView = itemView.value_activity_value
        val unit: TextView = itemView.value_activity_unit

        fun bind(activity: Activity) {
            name.text = activity.name
            value.text = activity.value.toString()
            unit.text = activity.measurementUnit
        }

    }

}