package com.example.rma_projekat2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rma_projekat2.database.entites.Category
import kotlinx.android.synthetic.main.category_item.view.*

class CategoryAdapter(
    private val categoryList: List<Category>,
    private val itemClickListener: (Category) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.category_item,
            parent, false
        )

        return CategoryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val currentItem = categoryList[position]

        holder.bind(currentItem)
        holder.itemView.setOnClickListener{ itemClickListener(currentItem) }
    }

    override fun getItemCount() = categoryList.size

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivKategorije: TextView = itemView.naziv_kategorije
        val opisKategorije: TextView = itemView.opis_kategorije
        val osobinaKategorije: TextView = itemView.osobina_kategorije

        fun bind(category: Category) {
            nazivKategorije.text = category.name
            opisKategorije.text = category.category_type.name
            if (category.trait != null) {
                osobinaKategorije.text = "Trait: " + category.trait.name
            } else {
                osobinaKategorije.text = "Trait not added"
            }
        }
    }
}