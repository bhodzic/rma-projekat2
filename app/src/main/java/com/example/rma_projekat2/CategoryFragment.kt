package com.example.rma_projekat2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rma_projekat2.activityList.ActivityList
import com.example.rma_projekat2.database.DB
import com.example.rma_projekat2.database.entites.Category
import com.example.rma_projekat2.databinding.FragmentCategoryBinding
import kotlinx.android.synthetic.main.fragment_category.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class CategoryFragment : Fragment() {
    private val model: LocalViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentCategoryBinding>(
            inflater, R.layout.fragment_category, container, false
        )
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.category_title)

        binding.addCategoryButton.setOnClickListener { v: View ->
            v.findNavController().navigate(
                R.id.action_categoryFragment_to_categoryInputFragment
            )
        }
        return binding.root;

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val db: DB = DB.getInstance(requireContext())
        // Potrebno povuci iz baze umjesto rucno generisati

        val categoryList = db.mainDAO().getCategories()

        recyclerViewCategory.adapter = CategoryAdapter(categoryList) { c: Category ->
            // Toast.makeText(context, c.name, Toast.LENGTH_LONG).show()
            model.setCurrentCategory(c)
            // Otvoriti ActivityList fragment
            view?.findNavController()?.navigate(R.id.action_categoryFragment_to_activityList)
        }
        recyclerViewCategory.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewCategory.setHasFixedSize(true)
    }

    private fun generateDummyList(size: Int): List<CategoryItem> {
        val list = ArrayList<CategoryItem>()
        for (i in 0 until size) {
            val drawable = R.drawable.ic_category
            val item = CategoryItem(drawable, "Kategorija $i", "Opis 2")
            list += item
        }
        return list
    }

}
