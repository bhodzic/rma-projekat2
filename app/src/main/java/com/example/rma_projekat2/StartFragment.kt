package com.example.rma_projekat2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.rma_projekat2.databinding.FragmentStartBinding

class StartFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentStartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_start, container, false)
        binding.newCategory.setOnClickListener { v: View ->
            v.findNavController().navigate(R.id.action_startFragment_to_categoryFragment)
        }

        binding.newActivity.setOnClickListener { v: View ->
            v.findNavController().navigate(R.id.action_startFragment_to_insertActivityFragment)
        }

        binding.newCategory.setOnClickListener { v ->
            v.findNavController().navigate(R.id.action_startFragment_to_categoryInputFragment)
        }

        binding.myCategories.setOnClickListener { v ->
            v.findNavController().navigate(R.id.action_startFragment_to_categoryFragment)
        }
        return binding.root
    }
}