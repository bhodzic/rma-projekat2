package com.example.rma_projekat2.database

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.rma_projekat2.database.entites.Activity
import com.example.rma_projekat2.database.entites.Category
import com.example.rma_projekat2.database.entites.CategoryType

@Dao
interface MainDAO {
    @Query("SELECT * FROM category")
    fun getCategories(): List<Category>

    @Query("SELECT * FROM category")
    fun getCursorCategories(): Cursor

    @Query("SELECT * FROM activity WHERE category_id = :category")
    fun getActivitiesByCategory(category: Int): List<Activity>

    @Query("SELECT * FROM category_type")
    fun getCategoryTypes(): List<CategoryType>

    @Insert
    fun insertCategory(item: Category)

    @Insert
    fun insertCategories(list: List<Category>)

    @Insert
    fun insertCategoryTypes(list: List<CategoryType>)

    @Insert
    fun insertActivity(item: Activity)

    @Update
    fun updateActivity(activity: Activity)
}