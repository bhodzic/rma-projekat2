package com.example.rma_projekat2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.rma_projekat2.database.entites.*


@Database(
    entities = [Category::class, CategoryType::class, Trait::class, Activity::class],
    version = 1,
    exportSchema = false
)
abstract class DB : RoomDatabase() {
    abstract fun mainDAO(): MainDAO

    companion object {
        @Volatile
        private var INSTANCE: DB? = null
        val data = PrepopulateDB()

        fun getInstance(context: Context): DB {
            synchronized(this) {
                if (INSTANCE == null) {
                    INSTANCE = createInstance(context)
                }
                return INSTANCE!!
            }
        }

        private fun createInstance(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DB::class.java,
                "DataBase.db"
            )
                 .addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Thread(Runnable { prepopulateDb(getInstance(context)) }).start()
                    }
                })
                .allowMainThreadQueries()
                .build()

        private fun prepopulateDb(instance: DB) {
            instance.mainDAO().insertCategoryTypes(data.categoryTypeList)
            instance.mainDAO().insertCategories(data.categoryList)
        }
    }
}