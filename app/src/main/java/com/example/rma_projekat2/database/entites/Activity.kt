package com.example.rma_projekat2.database.entites

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "activity")
data class Activity (
     @PrimaryKey(autoGenerate = true)
     val id: Int,

    @ColumnInfo(name = "name")
    val name: String,

    // Ispod su nullable tipovi jer zavisi od category_type koji će se podaci unositi

     @ColumnInfo(name = "start")
     var start: Long?, // timestamp

     @ColumnInfo(name = "stop")
     var stop: Long?, // timestamp

     @ColumnInfo(name = "unit")
     var measurementUnit: String?,

     @ColumnInfo(name = "value")
     var value: Double?,

     @ColumnInfo(name = "step")
     var step: Int?,

     @ColumnInfo(name = "has_trait")
     var hasTrait: Boolean = false,

     @Embedded(prefix = "category_")
     val category: Category
)