package com.example.rma_projekat2.database

import com.example.rma_projekat2.database.entites.Category
import com.example.rma_projekat2.database.entites.CategoryType

class PrepopulateDB {
    val categoryTypeList: List<CategoryType> = listOf(
        CategoryType(
            1,
            "Inkrementalna"
        ),
        CategoryType(2, "Vremenska"),
        CategoryType(3, "Količinska")
    )

    val categoryList: List<Category> = listOf(
        Category(
            0,
            "Spavanje",
            categoryTypeList[1],
            null
        ),
        Category(
            0,
            "Težina",
            categoryTypeList[2],
            null
        ),
        Category(
            0,
            "Broj čaša vode",
            categoryTypeList[0],
            null
        )
    )
}