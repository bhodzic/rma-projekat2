package com.example.rma_projekat2.database.entites

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category")
data class Category (
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @Embedded(prefix = "type_")
    val category_type: CategoryType,

    @Embedded(prefix = "trait_")
    val trait: Trait?
)
