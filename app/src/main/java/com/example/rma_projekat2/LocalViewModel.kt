package com.example.rma_projekat2

import androidx.lifecycle.ViewModel
import com.example.rma_projekat2.database.entites.Activity
import com.example.rma_projekat2.database.entites.Category

class LocalViewModel() : ViewModel() {
    private lateinit var insertedActivity: Activity
    private lateinit var currentCategory: Category

    fun setInsertedActivity(activity: Activity) {
        insertedActivity = activity
    }

    fun getInsertedActivity() = insertedActivity

    fun setCurrentCategory(category: Category) {
        currentCategory = category
    }

    fun getCurrentCategory() = currentCategory
}